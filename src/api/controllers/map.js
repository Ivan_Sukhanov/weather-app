import { mapApiKey } from '@/api/config'

export const yandexMap = {
  init: function () {
    return new Promise((resolve, reject) => {
      if (window.ymaps) {
        const ymaps = window.ymaps
        resolve(ymaps)
      } else {
        const node = document.createElement('script')
        node.id = 'map-api'
        node.src = `https://api-maps.yandex.ru/2.1/?apikey=${mapApiKey}&lang=ru_RU`

        node.addEventListener('load', () => {
          const ymaps = window.ymaps
          ymaps.ready(() => resolve(ymaps))
        })
        node.onerror = (error) => reject(error)

        document.head.appendChild(node)
      }
    })
  },
}
