import { weatherApiKey } from '@/api/config'

export const getCitesList = async (query) => {
  const params = new URLSearchParams({
    q: query,
    appid: weatherApiKey,
    sort: 'population',
    units: 'metric',
    lang: 'ru',
  })

  return new Promise((resolve, reject) => {
    return fetch(`http://api.openweathermap.org/data/2.5/find?${params}`)
      .then((response) => response.json())
      .then((data) => resolve(data.list))
      .catch((error) => reject(error))
  })
}
