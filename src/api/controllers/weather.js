import { weatherApiKey } from '@/api/config'

export const getWeatherByCityId = async (id) => {
  const params = new URLSearchParams({
    id,
    appid: weatherApiKey,
  })

  return new Promise((resolve, reject) => {
    return fetch(`http://api.openweathermap.org/data/2.5/weather?${params}`)
      .then((response) => response.json())
      .then((data) => resolve(data))
      .catch((error) => reject(error))
  })
}

export const getWeatherByCoords = async (coords) => {
  const params = new URLSearchParams({
    lon: coords[1],
    lat: coords[0],
    appid: weatherApiKey,
  })

  return new Promise((resolve, reject) => {
    return fetch(`http://api.openweathermap.org/data/2.5/weather?${params}`)
      .then((response) => response.json())
      .then((data) => resolve(data))
      .catch((error) => reject(error))
  })
}
